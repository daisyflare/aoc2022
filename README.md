# AOC 2022

My solutions for [Advent of Code 2022](https://adventofcode.com/2022). I tried to write all of these in idiomatic, easy-to-read Rust both as an example (if anyone wants to look at these lol) and as proof that yes, I *can* code neatly, I just don't *want* to.

## Running

``` sh
$ cargo run --bin <day>
```

`<day>` is in the format `01-1`, `01-2`, `02-1`, etc. The first part is the actual day, the second part is whether you want to do the first problem or the second.

Requires [Rust](https://rustup.rs) to run.
