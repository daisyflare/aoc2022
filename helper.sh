#!/usr/bin/env sh

case "$1" in
     "clone")
         cd ./src

         num="$(ls | grep -v -e "\-2" -e "lib\.rs" | sed "s/\-1\.rs//g" | sort | tail -1)"
         cp "$num-1.rs" "$num-2.rs"
         echo "Successfully copied src/$num-1.rs to src/$num-2.rs"

         name="$num-2"
         ;;
     *)
         cd ./src

         echo "use preload::*;"              >> "$1-1.rs"
         echo "fn main() {"                  >> "$1-1.rs"
         echo "    let input = get_input();" >> "$1-1.rs"
         echo "}"                            >> "$1-1.rs"

         echo "Successfully generated $1-1.rs"

         name="$1-1"
         ;;
esac

cd ..

printf "\n"                    >> Cargo.toml
echo "[[bin]]"                 >> Cargo.toml
echo "path = \"src/$name.rs\"" >> Cargo.toml
echo "name = \"$name\""        >> Cargo.toml
