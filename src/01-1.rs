use preload::get_input;

fn main() {
    let input = get_input();

    let mut max = 0;
    let mut running_total = 0;

    for line in input.lines() {
        if line.trim() == "" {
            max = running_total.max(max);
            running_total = 0;
            continue;
        }

        running_total += line.trim().parse::<usize>().unwrap();
    }

    max = running_total.max(max);

    println!("The elf holding the maximum calories holds {max}!");
}
