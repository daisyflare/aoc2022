use preload::get_input;

fn main() {
    let input = get_input();

    let [mut a, mut b, mut c] = [0; 3];
    let mut running_total = 0;

    for line in input.lines() {
        if line.trim() == "" {
            (a, b, c) = chain_max(running_total, a, b, c);
            running_total = 0;
            continue;
        }

        running_total += line.trim().parse::<usize>().unwrap();
    }

    (a, b, c) = chain_max(running_total, a, b, c);

    println!("The three holding the maximum calories hold {}!", a + b + c);
}

fn chain_max(new: usize, a: usize, b: usize, c: usize) -> (usize, usize, usize) {
    assert!(a >= b);
    assert!(b >= c);

    if new > a {
        (new, a, b)
    } else if new > b {
        (a, new, c)
    } else if new > c {
        (a, b, new)
    } else {
        (a, b, c)
    }
}
