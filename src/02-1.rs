use preload::*;
#[derive(Clone, Copy, Eq, PartialEq)]
#[repr(u8)]
enum Shape {
    Rock = 1,
    Paper = 2,
    Scissors = 3,
}

impl Shape {
    fn wins(&self, other: &Shape) -> bool {
        match (self, other) {
            (Rock, Rock) => false,
            (Rock, Paper) => false,
            (Rock, Scissors) => true,
            (Paper, Rock) => true,
            (Paper, Paper) => false,
            (Paper, Scissors) => false,
            (Scissors, Rock) => false,
            (Scissors, Paper) => true,
            (Scissors, Scissors) => false,
        }
    }
}

use Shape::*;

fn main() {
    let input = get_input();

    let mut total = 0;

    for line in input.lines() {
        let (opponent, you) = parse_line(line);

        let outcome = if opponent == you {
            // tie
            3
        } else if you.wins(&opponent) {
            // win
            6
        } else {
            // loss
            0
        };

        total += outcome + you as usize;
    }

    println!("The total score is {total}!");
}

fn parse_line(line: &str) -> (Shape, Shape) {
    let (opponent, you) = line.trim().split_once(' ').unwrap();
    let opponent = match opponent {
        "A" => Rock,
        "B" => Paper,
        "C" => Scissors,
        _ => unreachable!(),
    };
    let you = match you {
        "X" => Rock,
        "Y" => Paper,
        "Z" => Scissors,
        _ => unreachable!(),
    };
    (opponent, you)
}
