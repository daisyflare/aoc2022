use preload::*;

#[derive(Clone, Copy, Eq, PartialEq)]
#[repr(u8)]
enum Shape {
    Rock = 1,
    Paper = 2,
    Scissors = 3,
}

impl Shape {
    fn winner(&self) -> Shape {
        match self {
            Rock => Paper,
            Paper => Scissors,
            Scissors => Rock,
        }
    }
    fn loser(&self) -> Shape {
        match self {
            Rock => Scissors,
            Paper => Rock,
            Scissors => Paper,
        }
    }
}

use Shape::*;

fn main() {
    let input = get_input();

    let mut total = 0;

    for line in input.lines() {
        let (opponent, outcome) = parse_line(line);

        let (outcome_score, you) = match outcome {
            "X" => (0, opponent.loser()),
            "Y" => (3, opponent),
            "Z" => (6, opponent.winner()),
            _ => unreachable!(),
        };

        total += outcome_score + you as usize;
    }

    println!("The total score is {total}!");
}

fn parse_line(line: &str) -> (Shape, &str) {
    let (opponent, you) = line.trim().split_once(' ').unwrap();
    let opponent = match opponent {
        "A" => Rock,
        "B" => Paper,
        "C" => Scissors,
        _ => unreachable!(),
    };
    (opponent, you)
}
