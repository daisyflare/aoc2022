use preload::*;
use std::collections::HashSet;

fn main() {
    let input = get_input();

    let mut total = 0;

    for line in input.lines() {
        let len = line.trim().len();
        let (a, b) = line.trim().split_at(len / 2);

        for c in a
            .chars()
            .collect::<HashSet<_>>()
            .intersection(&b.chars().collect())
        {
            assert!(c.is_ascii() && c.is_alphabetic());

            let priority = if c.is_uppercase() {
                *c as usize - 38
            } else {
                *c as usize - 96
            };

            total += priority;
        }
    }

    println!("The total is {total}!")
}
