use preload::*;
use std::collections::HashSet;

fn main() {
    let input = get_input();

    let mut buf = [None, None];
    let mut total = 0;

    for line in input.lines() {
        if let [None, None] = buf {
            buf[0] = Some(line);
            continue;
        } else if let [Some(_), None] = buf {
            buf[1] = Some(line);
            continue;
        }

        let a = buf[0].take().unwrap();
        let b = buf[1].take().unwrap();

        let mut intersection: Vec<char> = a
            .trim()
            .chars()
            .collect::<HashSet<char>>()
            .intersection(&b.trim().chars().collect())
            .cloned()
            .collect::<HashSet<char>>()
            .intersection(&line.trim().chars().collect())
            .cloned()
            .collect();

        assert_eq!(intersection.len(), 1);

        let c = intersection.pop().unwrap();

        assert!(c.is_alphabetic());

        total += if c.is_uppercase() {
            c as usize - 38
        } else {
            c as usize - 96
        };
    }

    println!("The total is {total}!")
}
