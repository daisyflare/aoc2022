use std::{
    env, fs,
    io::{self, Read},
};

pub fn get_input() -> String {
    env::var("INPUT")
        .map(|x| fs::read_to_string(x).unwrap())
        .unwrap_or_else(|_| {
            let mut args = env::args();
            args.next(); // Program name
            let arg = match args.next() {
                Some(s) => fs::read_to_string(s).unwrap(),
                None => {
                    println!("Type input in, then press CTRL+D:");
                    let mut stdin = io::stdin().lock();
                    let mut s = String::new();
                    stdin.read_to_string(&mut s).unwrap();
                    s
                }
            };
            arg.trim().to_string()
        })
}
